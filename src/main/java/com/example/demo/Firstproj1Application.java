package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Firstproj1Application {

	public static void main(String[] args) {
		System.out.println("started..");
		SpringApplication.run(Firstproj1Application.class, args);
	}

}
