package com.example.demo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AlienRepo extends JpaRepository<Alien, Integer>{

	List<Alien> findByAname(String aname); //Query DSL(domain specific language ) JPA provides query by param name
	
	List<Alien> findByAnameOrderByAid(String aname);
	
	@Query("from Alien where aname= :name")
	List<Alien> find(@Param("name") String aname);
}
